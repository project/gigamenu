
Drupal.behaviors.GigaMenu = function(context) {
  // MouseOver event handler.
  var hoverOn = function() {
    $(this).find('ul.gigamenu:first')
      .fadeIn('fast');
  };

  // MouseOut event handler.
  var hoverOut = function() {
    $(this).find('ul.gigamenu:first')
      .fadeOut('fast');
  };

  // Hide giga menus.
  $('ul.gigamenu li.gigamenu-level-0 > ul.gigamenu').hide();

  // Attach behavior to all giga menu parent items that have a giga menu.
  $('ul.gigamenu li.gigamenu-level-0:has(ul.gigamenu)').hoverIntent(hoverOn, hoverOut);
};
