
Giga Menu Module
--------------------------------------------------------------------------------
The Giga Menu module enables a standard Drupal menu to be displayed as a Mega
Menu.

Mega Menus are menus in which sub-items are displayed all at once, giving users
access to the whole navigation menu tree, and thereby reducing the number of
clicks required to navigate around a website.

Project page
--------------------------------------------------------------------------------

http://drupal.org/project/gigamenu

Issue queue
--------------------------------------------------------------------------------

http://drupal.org/project/issues/gigamenu

Dependency
--------------------------------------------------------------------------------

hoverIntent module - http://drupal.org/project/hoverintent 

Installation
--------------------------------------------------------------------------------

Download hoverIntent and Giga Menu modules and extract their files to your
modules folder (e.g. sites/all/modules).

Go to admin/build/modules and enable both hoverIntent and Giga Menu modules.

Go to admin/build/menu and click to edit a menu or add a new menu. You will now
see there's a "Giga menu" section on the menu editing form. Check the "Make this
menu a giga menu" checkbox. By doing this a new block will be made available for
you to choose where you want it displayed. You can also choose to have it as a
vertical or a horizontal menu.

Now, navigate to admin/build/block and add the new created block to some region
of your theme. It'll show up as "Giga menu: [menu_name]" on the block
administration screen .

Note:

Primary/Secondary navigation is currently not supported out-of-the-box.

It should be possible to make this work with primary/secondary navigation but
may require some work on the theme, to override the variable content for these
menus. This steel needs to be tested and documented properly.
